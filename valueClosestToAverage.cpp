//============================================================================
// Name        : valueClosestToAverage.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cmath>
using namespace std;

void fillArrayWithConsecutiveValues(int *array, int size);
double calculateAverage(int *array, int size);
double findClosestToAverage(int *array, int size);

int main() {

	const int arraySize (20);

	cout << "Program returns first value from array closest to average." << endl;
	int array[arraySize];
	fillArrayWithConsecutiveValues(array, arraySize);
	double closestToAverage = findClosestToAverage(array, arraySize);
	cout << "Value closest to average is: " << closestToAverage << endl;
	return 0;
}

void fillArrayWithConsecutiveValues(int *array, int size) {
	for (int i = 0; i < size; i++) {
		array[i] = i;
	}
}
double calculateAverage(int *array, int size) {
	int counter(0);
	double sum(0);
	for (int i = 0; i < size; i++, counter++) {
		sum += array[i];
	}
	return sum /= counter;
}
double findClosestToAverage(int *array, int size) {
	double average = calculateAverage(array, size);
	int closestToAverage = array[0];
	double smallestDifference = abs(average - array[0]);
	for (int i = 1; i < size; i++) {
		double temp = abs(average - array[i]);
		if (abs(temp) < smallestDifference) {
			closestToAverage = array[i];
			smallestDifference = temp;
		}
	}
	return closestToAverage;
}
